var $j = jQuery.noConflict();

$j(document).ready(function() {
	/* Search Box - clears the "Search the mall..." text when user clicks on it. */
	$j("#search input.text").focus(function(){
		if ($j(this).val() == "Search the mall...") {
			$j(this).val("");
		}
	});
	$j("#search input.text").blur(function(){
		if ($j(this).val() == "") {
			$j(this).val("Search the mall...");
		}
	});
	/* Search Box Signup Form - clears the "Join our e-mail list here..." text when user clicks on it. */
	$j("#mailing input.text").focus(function(){
		if ($j(this).val() == "Join our email list here...") {
			$j(this).val("");
		}
	});
	$j("#mailing input.text").blur(function(){
		if ($j(this).val() == "") {
			$j(this).val("Join our email list here...");
		}
	});
	/* Homepage Slideshow Cylce */
	$j("#main.main-splash").cycle({
		timeout: 6000
	});
	/* Store Photo Gallery */
	$j("#store-gallery").after("<div id=\"pager\">").cycle({
		timeout: 6000,
		pager: "#pager"
	});
	/* Set a standard height to all the tabs */
	var storeFolder = 0;
	$j("#store-tabsection div.store-folder > div").each(function(){
		if ($j(this).height() > storeFolder) {
			storeFolder = $j(this).height() + 70;
			$j("#store-tabsection div.store-folder").css("height", storeFolder + "px");
		}
		$j(this).css("display", "none");
	});
	/* Store Tab Section */
	$j("#store-tabsection div.store-tabs a").click(function () {
		$j(this).parent().parent().children("li").removeClass("store-tabs-active");
		$j(this).parent().addClass("store-tabs-active");
		var newclass = $j(this).attr("class");
		newclass = newclass.replace("tabs", "folder");
		$j("#store-tabsection div.store-folder div").removeClass("store-folder-active");
		$j("#store-tabsection div.store-folder").children("." + newclass).addClass("store-folder-active");
		return false;
	});

    //hide any flash boxes that were shown via html
    $j('#flash_success').delay(5000).hide('blind');
    $j('#flash_notice').delay(5000).hide('blind');
    $j('#flash_error').delay(5000).hide('blind');
});

var js_flash = function(id, msg){
   //new Effect.Appear(id, { duration: 1, queue: { position: 'front', scope: 'flashscope'} });
   $j('#'+id).show('blind').delay(5000).hide('blind');
   /* new Effect.Fade(id, { duration: 2, queue: { position: 'back', scope: 'flashscope', delay: 10} });  */
};
