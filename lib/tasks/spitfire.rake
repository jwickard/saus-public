
namespace :spitfire do
  task :dbcycle do
    puts 'Dropping Database'
    Rake::Task["db:drop"].invoke

    puts 'Creating Database'
    Rake::Task["db:create"].invoke

    puts 'Creating Tables'
    Rake::Task["db:migrate"].invoke

    puts 'Seeding Database'
    Rake::Task["db:seed"].invoke
  end
end