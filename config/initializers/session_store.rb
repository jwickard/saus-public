# Be sure to restart your server when you modify this file.

# Your secret key for verifying cookie session data integrity.
# If you change this key, all old sessions will become invalid!
# Make sure the secret is at least 30 characters and all random, 
# no regular words or you'll be exposed to dictionary attacks.
ActionController::Base.session = {
  :key         => '_SAUS_session',
  :secret      => '05b03ecaf33c4339ee4fc90e44cf2ac2b50e625b4f127d10d0fa6c0bb074f836a8825719062d1a69d26c8717b173e65d7d777504d96174aaee22ac0d8cba1854'
}

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rake db:sessions:create")
# ActionController::Base.session_store = :active_record_store
