ActionController::Routing::Routes.draw do |map|
  map.resources :cms_pages

  map.resources :menus

  map.resources :events

  map.resources :job_listings

  map.resources :news_items

  # The priority is based upon order of creation: first created -> highest priority.

  # Sample of regular route:
  #   map.connect 'products/:id', :controller => 'catalog', :action => 'view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   map.purchase 'products/:id/purchase', :controller => 'catalog', :action => 'purchase'
  # This route can be invoked with purchase_url(:id => product.id)
  map.logout 'logout', :controller => 'user_sessions', :action => 'destroy'
  map.login 'login', :controller => 'user_sessions', :action => 'new'
  map.register 'register', :controller => 'users', :action => 'new'
  map.export 'export_email', :controller => 'admin', :action => 'export_email'

  #Business urls
  map.hotels_directory 'hotels/', :controller => 'businesses', :action => 'hotels_directory'
  map.dining_directory 'dining/', :controller => 'businesses', :action => 'dining_directory'
  map.shops_directory 'shopping/', :controller => 'businesses', :action => 'shops_directory'
  map.shops_category_filter 'shopping/:category', :controller => 'businesses', :action => 'filter_directory'
  map.restaurant_category_filter 'dining/:category', :controller => 'businesses', :action => 'filter_directory'
  map.dining_detail 'dining/:slug.html', :controller => 'businesses', :action => 'show'
  map.shop_detail 'shopping/:slug.html', :controller => 'businesses', :action => 'show'
  map.hotel_detail 'hotels/:slug.html', :controller => 'businesses', :action => 'show'

  map.cms_page 'page/:slug.html', :controller => :cms_pages, :action => :show

  #cms urls
  map.tos 'terms-of-service', :controller => 'cms_pages', :action => 'show', :slug => 'tos'
  map.privacy 'privacy-policy', :controller => 'cms_pages', :action => 'show', :slug => 'privacy'
  map.press 'press-and-media-relations', :controller => 'cms_pages', :action => 'show', :slug => 'press'

  #asset urls
  map.banner_index 'admin/banners/index.html', :controller => :cms, :action => :banner_image_index
  map.banner_edit 'admin/banners/edit/:id', :controller => :cms, :action => :banner_image_edit
  map.banner_update 'admin/banners/update', :controller => :cms, :action => :banner_image_update
  map.banner_new 'admin/banners/new', :controller => :cms, :action => :banner_image_new
  map.banner_create 'admin/banners/create', :controller => :cms, :action => :banner_image_create
  map.banner_destroy 'admin/banners/destroy/:id', :controller => :cms, :action => :banner_image_destroy


  #slideshow urls
  map.create_slideshow 'slideshow/create', :controller => 'cms', :action => 'create_slide_show'
  map.edit_slideshow 'slideshow/edit/:id', :controller => 'cms', :action => 'edit_slide_show'
  map.destroy_slide 'slideshow/destroy/:id', :controller => 'cms', :action => 'destroy_slide'
  map.resort_slideshow 'slideshow/resort/:id', :controller => 'cms', :action => 'resort_slide_show'
  map.preview_slideshow 'slideshow/preview/:id', :controller => 'cms', :action => 'slide_show_preview'
  map.new_slideshow 'slideshow/:type/new', :controller => 'cms', :action => 'new_slide_show'
  #youtube urls
  map.new_video 'video/new', :controller => 'cms', :action => 'new_video'
  map.edit_video 'video/edit/:id', :controller => 'cms', :action => 'edit_video'

  #mall urls
  map.search 'search', :controller => 'businesses', :action => 'search'
  map.mall_directory 'directory.html', :controller => 'mall', :action => 'directory'
  map.mall_home 'home.html', :controller => 'mall', :action => 'index'
  map.mall_location 'location.html', :controller => :cms_pages, :action => :show, :slug => 'location'
  map.mall_links 'links.html', :controller => :cms_pages, :action => :show, :slug => 'area_links'
  map.mall_about 'about.html', :controller => :cms_pages, :action => :show, :slug => 'about'
  map.mall_parking 'parking.html', :controller => :cms_pages, :action => :show, :slug => 'parking'
  map.mall_contact 'contact-us.html', :controller => 'mall', :action => 'contact_us'
  map.mall_hours 'hours.html', :controller => :cms_pages, :action => :show, :slug => 'mall_hours'
  map.guest_services 'guest-services.html', :controller => :cms_pages, :action => :show, :slug => 'guest_services'
  map.media 'media.html', :controller => 'mall', :action => 'media'
  map.subscribe_to_mailing_list 'mailinglist/subscribe', :controller => 'mall', :action => 'subscribe_to_mailing_list'
  map.export_mailing_list 'mailinglist/export', :controller => 'mall', :action => 'export_mailing_list'

  #admin urls
  map.admin_new_business 'admin/business/new', :controller => :businesses, :action => 'admin_new_business'
  map.admin_business_index 'admin/shops/index.html', :controller => :businesses, :action => :admin_business_index
  map.admin_toggle_active 'admin/business/:id/toggle/:status', :controller => :businesses, :action => :admin_toggle_business
  map.admin_users_index 'admin/users/index.html', :controller => 'mall', :action => 'users_index'

  #cms urls
  map.edit_promo 'admin/promo/edit/:slug', :controller => :cms, :action => :edit_promo
  map.update_promo 'admin/promo/update', :controller => :cms, :action => :update_promo

  #sitemap routes.
  map.mall_sitemap 'sitemap.:format', :controller => "mall", :action => "sitemap"

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   map.resources :products
  map.resource :user_session
  map.resource :account, :controller => "users"

  # Sample resource route with options:
  #   map.resources :products, :member => { :short => :get, :toggle => :post }, :collection => { :sold => :get }
  map.resources :users
  # Sample resource route with sub-resources:
  #   map.resources :products, :has_many => [ :comments, :sales ], :has_one => :seller
  
  # Sample resource route with more complex sub-resources
  #   map.resources :products do |products|
  #     products.resources :comments
  #     products.resources :sales, :collection => { :recent => :get }
  #   end

  # Sample resource route within a namespace:
  #   map.namespace :admin do |admin|
  #     # Directs /admin/products/* to Admin::ProductsController (app/controllers/admin/products_controller.rb)
  #     admin.resources :products
  #   end

  # You can have the root of your site routed with map.root -- just remember to delete public/index.html.
  map.root :controller => "mall"

  # See how all your routes lay out with "rake routes"

  # Install the default routes as the lowest priority.
  # Note: These default routes make all actions in every controller accessible via GET requests. You should
  # consider removing or commenting them out if you're using named routes and resources.
  map.connect ':controller/:action/:id'
  map.connect ':controller/:action/:id.:format'
end
