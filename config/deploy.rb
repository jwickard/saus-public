set :user, 'jwickard'
set :domain, 'www.joelwickard.name'
set :application, "saus"
set :repository,  "git@#{user}.unfuddle.com:#{user}/#{application}.git"

# If you aren't deploying to /u/apps/#{application} on the target
# servers (which is the default), you can specify the actual location
# via the :deploy_to variable:
set :deploy_to, "/var/vhosts/shopsatuniversitysquare.com"

default_run_options[:pty] = true

# If you aren't using Subversion to manage your source code, specify
# your SCM below:
set :scm, 'git'
set :git_enable_submodules,1
set :debploy_via, :remote_cache
set :branch, 'master'
set :scm_verbose, true
set :use_sudo, false

role :app, domain
role :web, domain
role :db,  domain, :primary => true

namespace :deploy do
  task :restart do
    run "touch #{current_path}/tmp/restart.txt"
  end
end

Dir[File.join(File.dirname(__FILE__), '..', 'vendor', 'gems', 'hoptoad_notifier-*')].each do |vendored_notifier|
  $: << File.join(vendored_notifier, 'lib')
end

require 'hoptoad_notifier/capistrano'
