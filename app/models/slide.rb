class Slide < Document
  belongs_to :slide_show, :counter_cache => true

  acts_as_list :scope => :slide_show

  has_attached_file :doc,
                    :whiny_thumbnails => true,
                    :url => "/system/:class/:id/:style/:filename",
                    :styles => { :thumbnail => '222x100#',
                                 :admin_thumbnail => '100x56#',
                                 :slide => Proc.new { |slide| slide.gallery_size } },
                    :default_style => :slide

  def gallery_size
    "#{slide_show.width}x#{slide_show.height}#"
  end
end
