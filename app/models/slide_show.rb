class SlideShow < ActiveRecord::Base
  has_many :slides, :order => :position

  named_scope :enabled_with_slides, :conditions => ['slides_count > 0 AND enabled = ?', true]
  named_scope :disabled_or_empty, :conditions => ['slides_count = 0 OR enabled = ?', false]

  before_save :generate_slug

  def resort_slides(ids)
    self.slides.each do |slide|
      slide.position = ids.index(slide.id.to_s)+1
      slide.save!
    end
  end

  def display_thumb
    if self.slides.size > 0
      self.slides[0].doc.url(:thumbnail)
    else
      'default_business_logo.jpg'
    end
  end

  private

  def generate_slug
    if !self.name.nil?
      cleansed = self.name.gsub(/[ ]/, '-')
      cleansed = cleansed.gsub(/[^\w-]/, '')
      self.slug = cleansed.downcase
    end
  end
end
