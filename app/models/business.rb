class Business < ActiveRecord::Base
  has_many :business_hours_entries
  has_many :job_listings
  has_many :news_items, :conditions => {:enabled => true}
  has_one :business_gallery
  has_many :events
  has_one :logo
  has_and_belongs_to_many :users

  #validators

  before_save :generate_slug
  
  #method returns hours model for todays operation hours
  def todays_hours 
    now = Time.now
    special = business_hours_entries.find(:first, :conditions => ["override_date = CURRENT_DATE()",])
    
    #give back the overriden date time.
    return special if not special.nil?
      
    #other wise lets instead find the entry matching the day of the week. or null
    business_hours_entries.find(:first, :conditions => ["day_of_week = ?", now.wday])  
  end
  
  def active_job_listings
    job_listings.find(:all, :conditions => ["enabled = ?", true], :order => 'listed_at')
  end

  def active_news_items
    news_items.find(:all, :conditions => ["enabled = ?", true], :order => 'published_at')
  end

  def uploaded_data
    nil
  end

  def uploaded_data=(data)
    self.logo = Logo.new(:doc => data)
  end

  def logo_path
    if not self.logo.nil?
      self.logo.doc.url
    else
      'default_business_logo.jpg'
    end
  end

  def has_active_job_listings
    job_listings.count(:conditions => ['enabled = ?', true]) > 0
  end

  def has_active_news_items
    news_items.count(:conditions => ['enabled = ?', true]) > 0
  end

  def has_events
    events.size > 0
  end

  private

  def generate_slug
    if !self.name.nil?
      cleansed = self.name.gsub(/[^- \w]/, '')
      cleansed = cleansed.gsub(/[ ]+/, '-')
      self.slug = cleansed.downcase
    end
  end
end
