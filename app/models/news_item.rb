class NewsItem < ActiveRecord::Base
  belongs_to :business

  #validations
  validates_presence_of :title
  validates_presence_of :description
end
