class CmsPage < ActiveRecord::Base
  def self.is_published?(slug)
    result = CmsPage.find_by_sql(["SELECT p.published FROM cms_pages p WHERE p.slug = ?", slug])
    return false if result.empty?
    return result[0].published
  end
end
