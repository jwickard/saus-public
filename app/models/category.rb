class Category < ActiveRecord::Base
  has_and_belongs_to_many :businesses,
                          :join_table => 'business_categories_businesses',
                          :foreign_key => 'business_category_id',
                          :association_foreign_key => 'business_id'

  before_save :generate_slug

  #if we let active record associations do this we'll get either 2 or 1 + n queries depending on how we use find(), we can do it in one query.
  def self.find_with_count_by_type(type)
    Category.find_by_sql(['SELECT c.*, COUNT(bcb.business_id) as count FROM categories c LEFT JOIN business_categories_businesses bcb ON c.id = bcb.business_category_id LEFT JOIN businesses b ON b.id = bcb.business_id WHERE b.enabled = ? AND c.type = ? GROUP BY c.name', true, type])
  end

  private

  def generate_slug
    if !self.name.nil?
      cleansed = self.name.gsub(/[ ]/, '-')
      cleansed = cleansed.gsub(/[^\w-]/, '')
      self.slug = cleansed.downcase
    end
  end
end
