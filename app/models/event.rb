class Event < ActiveRecord::Base

  # Associations
  belongs_to :business
  has_one :coupon
  
end
