class MailingListSubmission < ActiveRecord::Base
  require 'csv'
  
  validates_presence_of :address, :message => 'Email Address Cannot Be Blank.'
  validates_format_of :address, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, :message => 'Email Address Must Be In A Valid Format'
  validates_uniqueness_of :address, :message => 'Email Address Has Already Been Added To Mailing List'

  def sync_to_mail_chimp

    if self.subscribed?.eql?(false) and self.subscribed_at.nil?
      #first time subscription, won't try to resubscribe if already unsubscribed.
      logger.info("Adding Address To Mailchimp list: #{MAILCHIMP[:list_name]}")
      chimp = Hominid::Base.new({:api_key => MAILCHIMP[:api_key]})
      chimp.subscribe(chimp.find_list_id_by_name(MAILCHIMP[:list_name]), self.address, {:email_type => 'html'})
      self.subscribed= true
      self.touch(:subscribed_at)
    end

    self.save
  end

  handle_asynchronously :sync_to_mail_chimp

  #TODO move this commented code to the action that will render the report
  #Here's the code to send the report in the action
  #send_data(report.read,:type => ‘text/csv; charset=iso-8859-1; header=present’,:filename => ‘report.csv’, :disposition =>’attachment’, :encoding => ‘utf8′)

  #static export method
  def self.to_csv
    entries = MailingListSubmission.find(:all, :order => 'address')

    report = StringIO.new
    CSV::Writer.generate(report, ',') do |row|
      row << ['id', 'address', 'subscribed', 'subscribed at', 'unsubscribed at', 'created at', 'updated at']

      entries.each do |entry|
        row << [entry.id, entry.address, entry.subscribed?, entry.subscribed_at, entry.unsubscribed_at, entry.created_at, entry.updated_at] 
      end
    end

    report.rewind
    return report
  end
end
