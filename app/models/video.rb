require 'open-uri'
class Video < ActiveRecord::Base
  has_one :video_thumbnail

  before_save :generate_thumb

  def thumbnail_url
    if not self.video_thumbnail.nil?
      self.video_thumbnail.doc.url
    else
      'default_business_logo.jpg'
    end
  end

  private

  def generate_thumb
    my_params = CGI::parse(self.full_url[self.full_url.index('?')+1,self.full_url.length])

    if my_params.has_key?('v')
      self.token = my_params['v'].to_s

      img = "http://img.youtube.com/vi/#{self.token}/0.jpg"

      #download actual remote image:
      io = open(URI.parse(img))
      def io.original_filename; base_uri.path.split("/").last; end
      io.original_filename.blank? ? nil : io

      self.video_thumbnail = VideoThumbnail.create({:doc => io})
    end

    #Create paperclip from remote image.
    #http://trevorturk.com/2008/12/11/easy-upload-via-url-with-paperclip/

    #parse param
    #
  end

end
