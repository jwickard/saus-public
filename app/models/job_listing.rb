class JobListing < ActiveRecord::Base
  belongs_to :business
  #validates_presence_of :title, :description, :business_id

  #before_create :set_default_listed_at

  private

  def set_default_listed_at
    if listed_at.nil?
      now = Time.now
      self.listed_at=now
      self.created_at=now
    end
  end
end
