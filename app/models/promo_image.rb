class PromoImage < Document
  has_one :promo

  has_attached_file :doc,
                    :url => "/system/:class/:id/:style/:filename",
                    :styles => { :image => '113x111#'},
                    :default_style => :image
end