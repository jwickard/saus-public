class Logo < Document
  belongs_to :business
  
  has_attached_file :doc,
                    :url => "/system/:class/:id/:style/:filename",
                    :styles => { :logo => '222x100#'},
                    :default_style => :logo
end