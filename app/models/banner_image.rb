class BannerImage < Document

  has_attached_file :doc,
                    :whiny_thumbnails => true,
                    :url => "/system/:class/:id/:style/:filename",
                    :styles => { :display => '712x65#' },
                    :default_style => :display
  
end
