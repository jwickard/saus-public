class RestaurantCategory < Category
  has_and_belongs_to_many :restaurants,
                          :join_table => 'business_categories_businesses',
                          :foreign_key => 'business_category_id',
                          :association_foreign_key => 'business_id'

  def self.find_all_with_count
    self.find_with_count_by_type('RestaurantCategory')
  end
end