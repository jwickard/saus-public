class VideoThumbnail < Document
  belongs_to :video

  has_attached_file :doc,
                    :url => "/system/:class/:id/:style/:filename",
                    :styles => { :thumbnail => '222x100#' },
                    :default_style => :thumbnail
end