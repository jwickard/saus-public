class Restaurant < Business
  #TODO can we move this generic categories definition up to the Business model?
  has_and_belongs_to_many :categories,
                          :join_table => 'business_categories_businesses',
                          :foreign_key => 'business_id',
                          :association_foreign_key => 'business_category_id'

  has_many :menus, :order => 'position'

  def detail_path
    return dining_detail_path(:id => self.id)
  end

  def has_menus
    return menus.size > 0
  end
end