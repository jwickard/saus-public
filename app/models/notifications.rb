class Notifications < ActionMailer::Base
  

  def contact_us(contact, sent_at = Time.now)
    subject    'Shops At University Square New Contact Us Form Submission'
    recipients SAUS[:contact_recipients]
    from       'noreply@shopsatuniversitysquare.com'
    sent_on    sent_at
    
    body       :contact => contact
  end

end
