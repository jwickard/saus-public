class Product < ActiveRecord::Base
  belongs_to :shop

  acts_as_list :scope => :shop
end
