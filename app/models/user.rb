class User < ActiveRecord::Base
  acts_as_authentic
  has_and_belongs_to_many :roles
  has_and_belongs_to_many :businesses

  #validations
  
  def has_role(role)
    found = roles.detect {|i| i.name == role}
    return !found.nil?
  end

  def in_roles?(roleset)
    roleset.each do |role|
      return true if has_role(role)
    end

    false
  end

  def can_edit_business?(business)
    match = businesses.detect { |i| i.id.eql?(business.id)}
    return ((has_role('store_admin') && !match.nil?) || has_role('mall_admin'))
  end

  def active?
    self.enabled == true
  end
end
