class Promo < ActiveRecord::Base
  belongs_to :promo_image

  def self.find_by_slug_with_promo_image(slug)
    Promo.find(:first, :conditions => ['slug = ?', slug], :include => :promo_image)
  end
end
