class ShopCategory < Category
  has_and_belongs_to_many :shops,
                          :join_table => 'business_categories_businesses',
                          :foreign_key => 'business_category_id',
                          :association_foreign_key => 'business_id'

  def self.find_all_with_count
    self.find_with_count_by_type('ShopCategory')
    #ShopCategory.find_by_sql("SELECT c.*, COUNT(scs.shop_id) as count FROM categories c LEFT JOIN shop_categories_shops scs ON c.id = scs.shop_category_id LEFT JOIN businesses b ON b.id = scs.shop_id WHERE b.enabled = 1 GROUP BY c.name")
  end
end