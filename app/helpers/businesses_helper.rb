module BusinessesHelper 
  def filter_path_for_type(category)
    if(category.instance_of?(ShopCategory))
      return shops_category_filter_path(:category => category.slug)
    elsif(category.instance_of?(RestaurantCategory))
      return restaurant_category_filter_path(:category => category.slug)
    end
  end
end
