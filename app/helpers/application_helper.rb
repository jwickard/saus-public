# Methods added to this helper will be available to all templates in the application.
module ApplicationHelper
  def has_access(role)
      return current_user.has_role(role) if !current_user.nil?
      return false
  end

  def cms_enabled(business)
    return false if current_user.nil?
    return current_user.can_edit_business?(business)
  end

  def show_cms_link?(slug)
    return CmsPage.is_published?(slug)
  end

  def basic_format(dt)
    return dt.strftime('%b %d')
  end

  def h_business_name(name)
    h(name).gsub("'", '&#39;')
  end

  def business_hours
    now = Time.now

    case now.wday
      when 0 : return "Sun - Noon - 5:00 pm"
      when 1..5 : return "#{now.strftime("%a")} 9:30 AM - 8:30 PM"
      when 6 : return "Sat - 9:30 AM - 5:30 PM" 
    end
  end
end
