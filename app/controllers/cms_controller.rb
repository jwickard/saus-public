class CmsController < ApplicationController
  layout 'main'

  cache_sweeper :media_sweeper, :only => [:update_video, :create_slide_show, :create_video, :destroy_slide, :resort_slide_show, :update_slide_show]
  
  def new_slide_show
    require_roles(['mall_admin', 'store_admin'])
    @slide_show = SlideShow.new()
    @type = params[:type]
  end

  def create_slide_show
    require_roles(['mall_admin', 'store_admin'])
    #check if slideshow was actually cancelled.
    if params[:commit].eql?('Cancel')
      flash[:success] = "Cancelled Gallery Creation"
      return redirect_to media_path
    end

    case params[:type]
      when "cms"
        @slide_show = CmsGallery.new(params[:slide_show])
      when "media"
        @slide_show = MediaGallery.new(params[:slide_show])
        @slide_show.width = 535
        @slide_show.height = 304
      else
        @slide_show = SlideShow.new(params[:slide_show])
    end

    if @slide_show.save
      if !params[:slide].nil?
        #TODO jinky, this is go get around proc in slide size
        slide_attributes = params[:slide]
        slide_attributes[:slide_show] = @slide_show
        @slide_show.slides << Slide.new(params[:slide])

        @slide_show.save
      end
      flash[:success] = "Image Gallery '#{@slide_show.name}' Successfully Created"
      redirect_to :action => :edit_slide_show, :id => @slide_show.id
    else
      render :action => :new_slide_show
    end
  end

  def edit_slide_show
    require_roles(['mall_admin', 'store_admin'])
    store_referer
    @slide_show = SlideShow.find(params[:id]).becomes(SlideShow)
  end

  def update_slide_show
    require_roles(['mall_admin', 'store_admin'])
    @slide_show = SlideShow.find(params[:slide_show][:id])

    #hack validation
    if !params[:slide].nil? and @slide_show.is_a?(BusinessGallery) and @slide_show.slides.size >= 4
      flash[:error] = "Can only upload a maximum of 4 slides for a business gallery"

      @slide_show = @slide_show.becomes(SlideShow)
      return render :action => :edit_slide_show
    end

    if @slide_show.update_attributes(params[:slide_show])
      #create the slide
      if !params[:slide].nil?
        #check to see if we're uploading too many slides
        if @slide_show.is_a?(BusinessGallery) and @slide_show.slides.size >= 4
          flash[:error] = "Can only upload a maximum of 4 slides for a business gallery"
          return render :action => :edit_slide_show
        else
          #TODO jinky, this is go get around proc in slide size
          slide_attributes = params[:slide]
          slide_attributes[:slide_show] = @slide_show
          @slide_show.slides << Slide.new(slide_attributes)
          @slide_show.save
        end
      end

      flash[:success] = 'Slide show was successfully updated.'
    end

    #check if slideshow was actually cancelled.
    if params[:commit].eql?('Done')
      redirect_back_or_default(root_path)
    else
      @slide_show = @slide_show.becomes(SlideShow)
      render :action => :edit_slide_show
    end
  end

  def resort_slide_show
    require_roles(['mall_admin', 'store_admin'])
    @slide_show = SlideShow.find(params[:id])
    @slide_show.resort_slides(params['slide-show-slides'])

    flash[:success] = 'Slide Sort Order Updated!'
  end

  def slide_show_preview
    @slide_show = SlideShow.find(params[:id])
    render :layout => false
  end

  def destroy_slide
    require_roles(['mall_admin', 'store_admin'])
    @image = Slide.find(params[:id])

    if @image.destroy
      flash[:success] = "Successfully Removed Slide!"
    end

    #we don't want to re-enter edit slideshow and set that as default.
    @slide_show = SlideShow.find(@image.slide_show_id)
    render :action => :edit_slide_show
  end

  #Youtube stuffs.
  def new_video
    @video = Video.new()
  end

  def create_video
    #let's save the youtube video.
    #check if form was cancelled.
    if params[:commit].eql?('Cancel')
      flash[:success] = "Cancelled Video Creation"
      return redirect_to media_path
    end

    @video = Video.new(params[:video])

    if @video.save
      #if !params[:thumbnail].nil?
      #  @video.video_thumbnail = video_thumbnail.rb.new(params[:thumbnail])

      #  @video.save
      #end
      flash[:success] = "Video '#{@video.name}' Successfully Created"
      redirect_to :action => :edit_video, :id => @video.id
    else
      render :action => :new_video
    end
  end

  def edit_video
    @video = Video.find(params[:id])
  end

  def update_video
    @video = Video.find(params[:video][:id])

    if @video.update_attributes(params[:video])
      flash[:success] = 'Video was successfully updated.'
    end

    #check if video was actually cancelled.
    if params[:commit].eql?('Done')
      redirect_to media_path
    else
      redirect_to :action => :edit_video, :id => @video.id
    end
  end

  def banner_image_new
    @banner_image = BannerImage.new
  end

  def banner_image_create
    if params[:commit].eql?('Cancel')
      flash[:success] = "Cancelled Banner Creation"
      return redirect_to banner_index_path
    end

    @banner_image = BannerImage.new(params[:banner_image])

    if @banner_image.save
      flash[:success] = "Banner Image was successfully created."
      redirect_to banner_index_path
    end
  end

  def banner_image_edit
    @banner_image = BannerImage.find(params[:id])
  end

  def banner_image_update
    if params[:commit].eql?('Cancel')
      flash[:success] = "Cancelled Banner Update"
      return redirect_to banner_index_path
    end

    @banner_image = BannerImage.find(params[:id])

    if @banner_image.update_attributes(params[:banner_image])
      flash[:success] = "Banner Images Successfully Updated!"
      redirect_to banner_index_path
    end
  end

  def banner_image_index
    @banners = BannerImage.find(:all)
  end

  def edit_promo
    @promo = Promo.find_by_slug(params[:slug])
  end

  def update_promo
    if params[:commit].eql?('Cancel')
      flash[:success] = "Cancelled Content Update"
      return redirect_to mall_home_path
    end

    @promo = Promo.find(params[:id])

    if @promo.update_attributes(params[:promo])
      flash[:success] = "Content Successfully Updated!"

      #check for image
      if not params[:doc].nil?
        #delete current image
        @promo.promo_image.destroy if not @promo.promo_image_id.nil?

        #add new image.
        @promo.promo_image = PromoImage.create({:doc => params[:doc]})

        @promo.save
      end

      #clean the footer cache
      expire_fragment('global_footer')


      redirect_to mall_home_path
    end
  end
end
