class JobListingsController < ApplicationController
  layout 'main'

  cache_sweeper :job_listing_sweeper, :only => [:create, :update, :destroy]

  # GET /job_listings/new
  def new
    #we expect to do this in the context of a business.
    @job_listing = JobListing.new({:business_id => params[:business_id]})

    respond_to do |format|
      format.html # new.html.erb
      format.js { render :layout => false }
    end
  end

  # GET /job_listings/1/edit
  def edit
    @job_listing = JobListing.find(params[:id])

    respond_to do |format|
      format.html
      format.js { render :layout => false }
    end
  end

  # POST /job_listings
  def create
    @job_listing = JobListing.new(params[:job_listing])

    @job_listing.enabled = true

    respond_to do |format|
      if @job_listing.save
        flash[:success] = 'Job Listing was successfully created.'
        format.html { redirect_to(@job_listing, :notice => 'JobListing was successfully created.') }
        format.js { render :layout => false }
      else
        format.html { render :action => "new" }
        format.js { render_js_validation_errors(@job_listing) }
      end
    end
  end

  # PUT /job_listings/1
  # PUT /job_listings/1.xml
  def update
    @job_listing = JobListing.find(params[:id])

    respond_to do |format|
      if @job_listing.update_attributes(params[:job_listing])
        flash[:success] = 'JobListing was successfully updated.'
        format.html { redirect_to(@job_listing, :notice => 'JobListing was successfully updated.') }
        format.js { render :layout => false }
      else
        format.html { render :action => "edit" }
        format.js { render_js_validation_errors(@job_listing) }
      end
    end
  end

  # DELETE /job_listings/1
  def destroy
    @job_listing = JobListing.find(params[:id])
    @job_listing.destroy

    flash[:success] = 'Job Listing was successfully removed.'

    respond_to do |format|
      format.html { redirect_to(job_listings_url) }
      format.js { render :layout => false }
    end
  end
end
