class CategoriesController < ApplicationController
  layout 'main'

  cache_sweeper :business_sweeper, :only => [:update_business_categories]
  
  def edit_business_categories
    @business = Business.find(params[:business_id])
    @available_categories = Category.find(:all, :conditions => ['type = ?', @business.class.name + 'Category'], :order => 'name')
  end

  def update_business_categories
    @business = Business.find(params[:business_id])

    if params[:business].nil?
      attributes = {:category_ids => {}}
    else
      attributes = params[:business]
    end

    respond_to do |format|
      if @business.update_attributes(attributes)
        flash[:success] = "#{@business.name} Categories Updated!"
        format.html { redirect_to business_detail_path_for_type(@business) }
        format.js
      end
    end
  end
end
