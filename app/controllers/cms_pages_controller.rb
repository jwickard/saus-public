class CmsPagesController < ApplicationController
  layout 'main'

  caches_action :show, :if => Proc.new { |controller| !controller.user_authenticated? }, :layout => false

  def show
    @cms_page = CmsPage.find_by_slug(params[:slug])

    if @cms_page.nil?
      flash[:error] = 'Page Does Not Exist'
      return redirect_back_or_default mall_home_path
    end

    if !@cms_page.title.nil? && !@cms_page.title.empty?
      @meta_title = @cms_page.title  
    end

    if !@cms_page.keywords.nil? && !@cms_page.keywords.empty?
      @meta_keywords = @cms_page.keywords
    end

    if !@cms_page.description.nil? && !@cms_page.description.empty?
      @meta_description = @cms_page.description
    end

    if @cms_page.is_a?(CmsBannerPage)
      @cms_page = @cms_page.becomes(CmsPage)
      render :template => 'cms_pages/show_cms_banner_page'
    end
  end

  # GET /cms_pages/1/edit
  def edit
    store_referer
    @cms_page = CmsPage.find(params[:id])
    @cms_page = @cms_page.becomes(CmsPage)
  end

  def update
    if params[:commit].eql?('Cancel')
      flash[:success] = "Canceled Page Update"
      return redirect_back_or_default media_path
    end

    @cms_page = CmsPage.find(params[:id])

    if @cms_page.update_attributes(params[:cms_page])
      flash[:success] = 'Updated Content'
      expire_action cms_page_url(:slug =>@cms_page.slug, :format => 'html')
      redirect_to( :action => 'show', :slug => @cms_page.slug)
    else
      render :action => "edit"
    end
  end
end
