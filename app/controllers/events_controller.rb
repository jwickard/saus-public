class EventsController < ApplicationController
  layout 'main'

  caches_action :index, :if => Proc.new { |controller| !controller.user_authenticated? }, :layout => false
  cache_sweeper :event_sweeper, :only => [:create, :update, :destroy]

  def index
    @events = Event.find(:all)
    #render :template => 'businesses/search'
  end

  # GET /events/new
  def new
    #we expect to do this in the context of a business.
    @event = Event.new({:business_id => params[:business_id]})

    respond_to do |format|
      format.html # new.html.erb
      format.js { render :layout => false }
    end
  end

  # GET /events/1/edit
  def edit
    @event = Event.find(params[:id])

    respond_to do |format|
      format.html #edit.html.erb
      format.js { render :layout => false }
    end
  end

  # POST /events
  # POST /events.xml
  def create
    @event = Event.new(params[:event])

    respond_to do |format|
      if @event.save
        flash[:success] = "Event was successfully created."
        format.html { redirect_to(@event, :notice => 'Event was successfully created.') }
        format.js  { render :layout => false }
      else
        format.html { render :action => "new" }
        format.js  { render_js_validation_errors(@event) }
      end
    end
  end

  # PUT /events/1
  # PUT /events/1.xml
  def update
    @event = Event.find(params[:id])

    respond_to do |format|
      if @event.update_attributes(params[:event])
        flash[:success] = 'Event was successfully updated.'
        format.html { redirect_to(@event, :notice => 'Event was successfully updated.') }
        format.js { render :layout => false }
      else
        format.html { render :action => "edit" }
        format.js { render_js_validation_errors(@event) }
      end
    end
  end

  # DELETE /events/1
  def destroy
    @event = Event.find(params[:id])
    @event.destroy
    flash[:success] = 'Event was successfully removed!'
    respond_to do |format|
      format.html { redirect_to(events_url) }
      format.js  { render :layout => false }
    end
  end
end
