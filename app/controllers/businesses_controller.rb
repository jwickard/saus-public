class BusinessesController < ApplicationController
  layout 'main'

  caches_action :shops_directory, :dining_directory, :hotels_directory, :show,
                 :if => Proc.new { |controller| !controller.user_authenticated? }, :layout => false
  cache_sweeper :business_sweeper, :only => [:add_product, :destroy_image, :destroy_product, :sort_products, :sort_images, :update_business_details, :update_business_hours, :update_business_info, :update_hotel_amenities, :upload_gallery_image, :admin_create_business, :admin_toggle_business]

  def hotels_directory
    @businesses = active_businesses_by_type(Hotel.model_name)
  end

  def dining_directory
    @categories = RestaurantCategory.find_all_with_count
    @businesses = active_businesses_by_type(Restaurant.model_name)
    @selected_category = RestaurantCategory.new({:slug => 'all', :name => 'All'})
    render :template => 'businesses/directory'
  end

  def shops_directory
    @categories = ShopCategory.find_all_with_count
    @businesses = active_businesses_by_type(Shop.model_name)
    @selected_category = ShopCategory.new({:slug => 'all', :name => 'All'})
    render :template => 'businesses/directory'
  end

  def filter_directory
    @selected_category = Category.find_by_slug(params[:category])
    @categories = Category.find_with_count_by_type(@selected_category.class.model_name)
    @businesses = @selected_category.businesses.find(:all, :conditions => ["enabled = ?", true], :order => 'name', :include => :logo)
    render :template => 'businesses/directory'
  end

  #TODO if we end up building anymore logic than loading a template by type then it's probably time to break these
  #out into their own actions / possibly controllers.
  def show
    @business = Business.find_by_slug(params[:slug])

    unless @business.nil?
      #Set page specific meta data.
      @meta_title = "The Shops at University Square | #{@business.name} Rochester MN"
      @meta_keywords = "#{@business.name}, #{@meta_keywords}"
      @meta_description = "#{@business.name} Details"

      render :template => 'businesses/business_detail'
      return
    end

    render :file => "#{RAILS_ROOT}/public/404.html",  :status => 404
  end

  #admin actions
  def edit_business_info
    @business = Business.find_by_id(params[:id])

    respond_to do |format|
      format.js { render :layout => false }
    end
  end

  def update_business_info
    @business = Business.find(params[:id])

    #force empty location to be nil
    if params[:business][:suite].empty?
      params[:business][:suite] = nil
    end

    if @business.update_attributes(params[:business])
      flash[:success] = "Business Information Updated!"
      redirect_to business_detail_path_for_type(@business)
    end
  end

  def edit_business_details
    @business = Business.find(params[:id])
    respond_to do |format|
      format.js { render :layout => false }
    end
  end

  def update_business_details
    @business = Business.find(params[:id])

    if @business.update_attributes(params[:business])
      flash[:success] = "Details Updated!"
      respond_to do |format|
        format.html { redirect_to business_detail_path_for_type(@business) }
        format.js { render :layout => false }
      end
    end
  end

  def edit_business_hours
    @business = Business.find(params[:id])
    respond_to do |format|
      format.js { render :layout => false }
    end
  end

  def update_business_hours
    @business = Business.find(params[:id])

    if @business.update_attributes(params[:business])
      flash[:success] = 'Hours Updated!'
      respond_to do |format|
        format.html { redirect_to business_detail_path_for_type(@business)}
        format.js { render :layout => false }
      end
    end
  end

  #TODO we could make this more generic.
  def edit_hotel_amenities
    @business = Business.find(params[:id])
    respond_to do |format|
      format.js { render :layout => false}
    end
  end

  def update_hotel_amenities
    @business = Business.find(params[:id])

    if @business.update_attributes(params[:business])
      flash[:success] = "Amenities Updated!"
      respond_to do |format|
        format.html { redirect_to business_detail_path_for_type(@business) }
        format.js { render :layout => false }
      end
    end
  end

  def edit_products
    @business = Business.find(params[:id])
    render :layout => false
  end

  def add_product
    @business = Shop.find(params[:id])
    @business.products << Product.create(params[:product])
    render :layout => false
  end

  def sort_products
    @business = Shop.find(params[:id])
    @business.products.each do |product|
      product.position = params['business_products'].index(product.id.to_s)+1
      product.save
    end

    render :text => "Sort Updated!"
  end

  def destroy_product
    @product = Product.find(params[:id])
    @business = @product.shop
    @product.delete

    flash[:success] = "Product Removed!"
  end

  def search
    #TODO this will turn the string 'search term' into 'searchterm'. Look at how this should be handled.
    criteria = "%#{params[:terms].to_s.split}%"
    
    @businesses = Business.find(:all,
      :conditions => ['enabled = ? AND (name LIKE ? OR description LIKE ? OR details LIKE ?)',
      true, criteria, criteria, criteria] )
    @events = Event.name_or_description_like(params[:terms].to_s.split)
  end
  
  def print_event
    @event = Event.find(params[:id])
    render :layout => 'coupon'
  end

  def admin_new_business
    @business = Business.new
    @user = User.new
  end

  def admin_create_business
    if params[:commit].eql?('Cancel')
      return render :action => :admin_business_index
    end

    @business = nil

    case params[:type]
      when 'Shop'
        @business = Shop.new(params[:business])
      when 'Restaurant'
        @business = Restaurant.new(params[:business])
      when 'Hotel'
        @business = Hotel.new(params[:business])
      else
        flash[:notice] = "cannot determine type for business"
        return render :action => :admin_new_business
    end

    #add a gallery!
    @business.business_gallery = BusinessGallery.create({:name => "#{@business.name} Gallery", :enabled => true, :width => 535, :height => 304})

    if @business.save
      flash[:success] = "#{@business.name} Successfully Created!"
      redirect_to business_detail_path_for_type(@business) #extra select forces cast
    else
      flash[:notice] = "There was a problem creating the business"
      render :action =>:admin_new_business
    end
  end

  def admin_toggle_business
    @business = Business.find(params[:id])

    if params[:status].eql?('true')
      @business.enabled = true
    elsif params[:status].eql?('false')
      @business.enabled = false
    end

    respond_to do |format|
      if @business.save
        flash[:success] = "Successfully #{@business.enabled ? "enabled" : "disabled"} #{@business.name}"
        format.html { redirect_to :action => :admin_business_index }
        format.js { render :layout => false }
      end
    end
  end

  def admin_business_index
    @businesses = Business.find(:all, :order => :name)
  end

  private

  def active_businesses_by_type(type)
    Business.find(:all, :conditions => ["enabled = ? AND type = ?", true, type], :order => 'name', :include => 'logo')
  end
end
