# Filters added to this controller apply to all controllers in the application.
# Likewise, all the methods added will be available for all controllers.

class ApplicationController < ActionController::Base
  helper :all # include all helpers, all the time
  helper_method :current_user_session, :current_user, :business_detail_path_for_type, :business_detail_url_for_type, :back_to_stores_by_type
  protect_from_forgery # See ActionController::RequestForgeryProtection for details

  before_filter :meta_defaults
  before_filter :layout_setup

  def user_authenticated?
    !current_user.nil?
  end

  # Scrub sensitive parameters from your log
  # filter_parameter_logging :password
  private 
  # begin authlogic support
  def current_user_session
    return @current_user_session if defined?(@current_user_session)
    @current_user_session = UserSession.find
  end

  def current_user
    return @current_user if defined?(@current_user)
    @current_user = current_user_session && current_user_session.user
  end

  def require_roles(roles)
    unless current_user && current_user.in_roles?(roles)
      store_location
      flash[:notice] = "You are not authorized to view this page."
      redirect_to login_url
      return false
    end
  end
  
  def require_user
    unless current_user
      store_location
      flash[:notice] = "You must be logged in to access this page"
      redirect_to login_url
      return false
    end
  end

  def require_no_user
    if current_user
      store_location
      redirect_to landing_page(current_user)
      return false
    end
  end
      
  def store_location
    session[:return_to] = request.request_uri
  end

  def store_referer
    session[:return_to] = request.referer
  end

  def redirect_back_or_default(default)
    redirect_to(session[:return_to] || default)
    session[:return_to] = nil
  end
  # end authlogic support

  def render_js_validation_errors(model)
    render :update do |page|
      page.alert model.errors.collect{|k,v| "The #{k} #{v}\n"}.to_s
    end
  end

  #TODO find a better way to do this.
  def back_to_stores_by_type(business, category = nil)
    if business.instance_of?(Shop)
      if not category.nil?
        return shops_category_filter_path(:category => category)
      else
        return shops_directory_path
      end
    elsif business.instance_of?(Restaurant)
      if not category.nil?
        return restaurant_category_filter_path(:category => category)
      else
        return dining_directory_path
      end
    elsif business.instance_of?(Hotel)
      return hotels_directory_path
    end
  end

  def business_detail_path_for_type(business)
    if (business.instance_of?(Shop))
      return shop_detail_path(:slug => business.slug)
    elsif (business.instance_of?(Restaurant))
      return dining_detail_path(:slug => business.slug)
    elsif (business.instance_of?(Hotel))
      return hotel_detail_path(:slug => business.slug)
    end
  end

  def business_detail_url_for_type(business)
    if(business.instance_of?(Shop))
      return shop_detail_url(:slug => business.slug)
    elsif(business.instance_of?(Restaurant))
      return dining_detail_url(:slug => business.slug)
    elsif(business.instance_of?(Hotel))
      return hotel_detail_url(:slug => business.slug)
    end
  end

  def landing_page(user)
    if user.has_role('store_admin') && !user.businesses.first.nil?
        business_detail_path_for_type(user.businesses.first)
    else
        mall_home_url
    end
  end

  def meta_defaults
    @meta_title = 'Shopping in Rochester MN - The Shops at University Square - Dining and Events in Downtown Rochester MN'
    @meta_keywords = 'Rochester, MN, Shopping, Dining, Events'
    @meta_description = 'Shopping in Rochester MN - The Shops at University Square - Dining and Events in Downtown Rochester MN'
  end

  def layout_setup
      @promo_one = Promo.find_by_slug_with_promo_image('promo_slot_1')
      @promo_two = Promo.find_by_slug_with_promo_image('promo_slot_2')
      @promo_three = Promo.find_by_slug_with_promo_image('promo_slot_3')
  end
end
