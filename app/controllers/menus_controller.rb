class MenusController < ApplicationController
  layout 'main'

  cache_sweeper :menu_sweeper, :only => [:create, :update, :destroy]

  # GET /menus/new
  def new
    #we are doing within the context of a restaurant.
    @menu = Menu.new({:restaurant_id => params[:restaurant_id]})

    respond_to do |format|
      format.html # new.html.erb
      format.js { render :layout => false }
    end
  end

  # GET /menus/1/edit
  def edit
    @menu = Menu.find(params[:id])

    respond_to do |format|
      format.html
      format.js { render :layout => false }
    end
  end

  # POST /menus
  def create
    @menu = Menu.new(params[:menu])

    respond_to do |format|
      if @menu.save
        flash[:success] = 'Menu was successfully created.'
        format.html { redirect_to(@menu, :notice => 'Menu was successfully created.') }
        format.js { render :layout => false }
      else
        format.html { render :action => "new" }
        format.js  { render_js_validation_errors(@menu) }
      end
    end
  end

  # PUT /menus/1
  def update
    @menu = Menu.find(params[:id])

    respond_to do |format|
      if @menu.update_attributes(params[:menu])
        flash[:success] = 'Menu was successfully updated.'
        format.html { redirect_to(@menu, :notice => 'Menu was successfully updated.') }
        format.js { render :layout => false }
      else
        format.html { render :action => "edit" }
        format.js { render_js_validation_errors(@menu) }
      end
    end
  end

  # DELETE /menus/1
  def destroy
    @menu = Menu.find(params[:id])
    @menu.destroy
    flash[:success] = 'Successfully removed Menu.'
    respond_to do |format|
      format.html { redirect_to(menus_url) }
      format.js { render :layout => false }
    end
  end
end
