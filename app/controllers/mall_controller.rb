class MallController < ApplicationController
  layout 'main'

  caches_action :guest_services, :location, :directory, :links, :about, :parking, :sitemap, :media,
                :if => Proc.new { |controller| !controller.user_authenticated? }, :layout => false

  #cache_sweeper :media_sweeper

  def index
    @gallery = CmsGallery.find_by_slug('front-page-gallery')
    @promo_one = Promo.find_by_slug('promo_slot_1')
  end

  def media
    #we only want to select slideshows with existing slides. TODO better way? (i.e. through finders)
    @galleries = MediaGallery.enabled_with_slides
    #don't bother with this lookup if we aren't in admin mode.
    @disabled_galleries = MediaGallery.disabled_or_empty if user_authenticated?
    @videos = Video.find(:all, :conditions => ['published = ? AND token IS NOT NULL', true])
    @disabled_videos = Video.find(:all, :conditions => ['published = ? OR token IS NULL', false])
  end

  def directory
    @first_floor_shops = Business.find(:all, :conditions => ["suite LIKE ? AND enabled = ?", '1%', true], :order => 'suite')
    @second_floor_shops = Business.find(:all, :conditions => ["suite LIKE ? AND enabled = ?", '2%', true], :order => 'suite')
    #@third_floor_shops = Business.find(:all, :conditions => ["suite LIKE ? AND enabled = ?", '3%', true])
    @other_shops = Business.find(:all, :conditions => ["suite IS NULL AND enabled = ?", true], :order => 'name')
  end

  def contact_us
    @contact = ContactSubmission.new    
  end

  def subscribe_to_mailing_list
    submission = MailingListSubmission.new({:address => params[:address]})

    if submission.save
      submission.sync_to_mail_chimp
      flash[:success] = "#{submission.address} added to our mailing list!"
    else
      errors = submission.errors
      flash[:error] = errors.collect { |error| error[1]}.join(', ')
    end

    respond_to do |format|
      format.html { redirect_to :back }
      format.js { render :layout => false } #renders rjs template
    end
  end

  def export_mailing_list
    require_roles('mall_admin')

    report = MailingListSubmission.to_csv
    send_data(report.read, :type => 'text/csv; charset=iso-8859-1; header=present', :filename => 'report.csv', :disposition => 'attachment', :encoding => 'utf8')
  end

  def process_contact_us
    @contact = ContactSubmission.new(params[:contact_submission])

    if @contact.save
      Notifications.send_later( :deliver_contact_us, @contact )
      #TODO subscribe them to newsletter!
    else
      #render the old action to display validation errors.
      render :action => 'contact_us'
    end
  end

  def sitemap
    @restaurants = Restaurant.find(:all, :order => 'name')
    @shops = Shop.find(:all, :order => 'name')
    #@hotels = Hotel.find(:all, :order => "created_on DESC")
    #@events = Event.find(:all, :order => "created_on DESC")

    respond_to do |format|
      format.xml  {} #sitemap.xml.builder
      format.html {} #sitemap.html.erb
    end
  end

    def users_index
      require_roles('mall_admin')
      #list all the current users at the mall.
      @users = User.find(:all, :order => 'last_request_at DESC')
    end

  def admin_new_user
    require_roles('mall_admin')
    @user = User.new
  end

  def admin_create_user
    require_roles('mall_admin')

    if params[:commit].eql?('Cancel')
      return redirect_to :action => :users_index
    end

    @user = User.new(params[:user])

    if @user.save
      flash[:success] = "User successfully created!"
      redirect_to :action => :users_index
    else
      flash[:error] = "Could not create user."
      render :action => :admin_new_user
    end
  end

  def admin_edit_user
    require_roles('mall_admin')
    @user = User.find(params[:id])
  end

  def admin_update_user
    require_roles('mall_admin')

    #user cancelled form.
    if params[:commit].eql?('Cancel')
      return redirect_to :action => :users_index
    end

    @user = User.find(params[:user][:id])
    logger.info("Trying to update user: #{@user.email}")
    #@user.validate_email_field(false)

    #associate user to business.

    if @user.update_attributes(params[:user])
      flash[:success] = 'User Updated!'
      redirect_to :action => :users_index
    else
      flash[:error] = "Problem Updating User"
      render :action => :admin_edit_user
    end
  end
end
