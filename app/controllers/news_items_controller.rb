class NewsItemsController < ApplicationController
  layout 'main'

  cache_sweeper :news_item_sweeper, :only => [:create, :update, :destroy]

  # GET /news_items/new
  def new
    #we expect to do this in the context of a business.
    @news_item = NewsItem.new({:business_id => params[:business_id]})

    respond_to do |format|
      format.html # new.html.erb
      format.js { render :layout => false } # new.js.rjs
    end
  end

  # GET /news_items/1/edit
  def edit
    @news_item = NewsItem.find(params[:id])

    respond_to do |format|
      format.html
      format.js { render :layout => false }
    end
  end

  # POST /news_items
  def create
    @news_item = NewsItem.new(params[:news_item])

    #TODO determine if we want this in the form instead.
    @news_item.enabled = true


    respond_to do |format|
      if @news_item.save
        flash[:success] = 'News was successfully created.'
        format.html { redirect_to(@news_item, :notice => 'NewsItem was successfully created.') }
        format.js { render :layout => false } # create.js.rjs
      else
        format.html { render :action => "new" }
        format.js { render_js_validation_errors(@news_item) }
      end
    end
  end

  # PUT /news_items/1
  def update
    @news_item = NewsItem.find(params[:id])

    respond_to do |format|
      if @news_item.update_attributes(params[:news_item])
        flash[:success] = 'News was successfully updated.'
        format.js { render :layout => false }
        format.html { redirect_to(@news_item, :notice => 'NewsItem was successfully updated.') }
      else
        format.js { render_js_validation_errors(@news_item) }
        format.html { render :action => "edit" }
      end
    end
  end

  # DELETE /news_items/1
  def destroy
    @news_item = NewsItem.find(params[:id])
    @news_item.destroy
    flash[:success] = 'News was successfully removed!'
    respond_to do |format|
      format.html { redirect_to(news_items_url) }
      format.js { render :layout => false }
    end
  end

end
