xml.instruct! :xml, :version=>"1.0"
xml.urlset(:xmlns => "http://www.sitemaps.org/schemas/sitemap/0.9") {

  xml.url {
    xml.loc(root_url)
    xml.lastmod(Time.now.localtime.strftime("%Y-%m-%d"))
    xml.changefreq("daily")
    xml.priority("0.25")
  }

  xml.url {
    xml.loc(mall_directory_url)
    xml.lastmod(Time.now.localtime.strftime("%Y-%m-%d"))
    xml.changefreq("daily")
    xml.priority("0.25")
  }

  xml.url {
    xml.loc(mall_location_url)
    xml.lastmod(Time.now.localtime.strftime("%Y-%m-%d"))
    xml.changefreq("daily")
    xml.priority("0.25")
  }

  xml.url {
    xml.loc(mall_links_url)
    xml.lastmod(Time.now.localtime.strftime("%Y-%m-%d"))
    xml.changefreq("daily")
    xml.priority("0.25")
  }

  xml.url {
    xml.loc(mall_about_url)
    xml.lastmod(Time.now.localtime.strftime("%Y-%m-%d"))
    xml.changefreq("daily")
    xml.priority("0.25")
  }

  xml.url {
    xml.loc(guest_services_url)
    xml.lastmod(Time.now.localtime.strftime("%Y-%m-%d"))
    xml.changefreq("daily")
    xml.priority("0.25")
  }

  for page in @restaurants
    xml.url do
      xml.loc(business_detail_url_for_type(page))
      xml.lastmod(page.created_at.strftime("%Y-%m-%d"))
      xml.changefreq("monthly")
      xml.priority("0.5")
    end
  end

  for page in @shops
    xml.url do
      xml.loc(business_detail_url_for_type(page))
      xml.lastmod(page.created_at.strftime("%Y-%m-%d"))
      xml.changefreq("monthly")
      xml.priority("0.5")
    end
  end
}