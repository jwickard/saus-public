class BusinessSweeper < BusinessCacheSweeper
  observe Business

  def after_create(business)
    expire_cache_for(business)
    expire_directory_cache(business)
  end

  def after_update(business)
    expire_cache_for(business)
    expire_directory_cache(business)
  end

  def after_destroy(business)
    expire_cache_for(business)
    expire_directory_cache(business)
  end

  #Product methods
  def after_businesses_add_product
    expire_cache_for(assigns(:business))
  end

  def after_businesses_destroy_product
    expire_cache_for(assigns(:business))
  end

  def after_businesses_sort_products
    expire_cache_for(assigns(:business))
  end

  #GalleryImage methods
  def after_businesses_upload_gallery_image
    expire_cache_for(assigns(:business))
  end

  def after_businesses_destroy_image
    expire_cache_for(assigns(:business))
  end

  def after_businesses_sort_images
    expire_cache_for(assigns(:business))
  end

  #business admin methods
  def after_businesses_admin_create_business
    #expire_cache_for(assigns(:business))
    expire_directory_cache(assigns(:business))
  end

  def after_businesses_admin_toggle_business
    expire_cache_for(assigns(:business))
    expire_directory_cache(assigns(:business))
  end

end