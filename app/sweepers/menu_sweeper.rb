class MenuSweeper < BusinessDependentObjectSweeper
  observe Menu

  def get_business_from_model(menu)
    return menu.restaurant
  end
end