class MediaSweeper < ActionController::Caching::Sweeper
  observe Video

  #video related sweeps
  def after_cms_create_video
    expire_media
  end

  def after_cms_update_video
    expire_media
  end

  #slideshow related sweeps
  def after_cms_create_slide_show
    expire_media
  end

  def after_cms_update_slide_show
    expire_media
  end

  def after_cms_resort_slide_show
    expire_media
  end

  def after_cms_destroy_slide
    expire_media
  end

  private

  def expire_media
    expire_action(media_url(:format => 'html'))
  end
end