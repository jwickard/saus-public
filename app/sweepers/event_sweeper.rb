class EventSweeper < BusinessCacheSweeper
  observe Event

  def after_create(event)
    expire_cache_for(event.business)
    expire_action(events_url)
  end

  def after_update(event)
    expire_cache_for(event.business)
    expire_action(events_url)
  end

  def after_destroy(event)
    expire_cache_for(event.business)
    expire_action(events_url)
  end
end