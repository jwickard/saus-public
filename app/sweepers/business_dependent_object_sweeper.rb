#Class implements boiler-plate CRUD sweeping for objects related to business.
class BusinessDependentObjectSweeper < BusinessCacheSweeper
  
  def after_create(model)
    expire_cache_for(get_business_from_model(model))
  end

  def after_update(model)
    expire_cache_for(get_business_from_model(model))
  end

  def after_destroy(model)
    expire_cache_for(get_business_from_model(model))
  end

  #callback that we can override to get business in a different way.
  def get_business_from_model(model)
    return model.business
  end

end