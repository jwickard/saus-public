class BusinessCacheSweeper < ActionController::Caching::Sweeper
  private

  def expire_cache_for(business)
    expire_action(:controller => 'businesses', :action => 'show', :slug => business.slug, :format => 'html')
  end

  def expire_directory_cache(business)
    if business.instance_of?(Shop)
      expire_action(shops_directory_url)
    elsif business.instance_of?(Restaurant)
      expire_action(dining_directory_url)
    elsif business.instance_of?(Hotel)
      expire_action(hotels_directory_url)
    end
  end

end