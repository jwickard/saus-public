require 'test_helper'

class UserTest < ActiveSupport::TestCase
  # Replace this with your real tests.
  
  test "has role" do
    #defined by our fixture data
    assert users(:user).has_role('user')
    #assert there was no role 'bleeding'
    assert !users(:user).has_role('store_admin')
    assert !users(:user).has_role('mall_admin')
    assert users(:store_admin).has_role('store_admin')
    assert users(:mall_admin).has_role('mall_admin')
    
    #test adding a role.
    r = Role.new({:name => 'test_unit', :display => 'Test Unit'})
    r.save
    
    #alias our 'standard' user
    testuser = users(:user)
    testuser.roles << r
    
    assert testuser.has_role('test_unit')
    
  end
end
