require 'test_helper'

class EmailListEntryTest < ActiveSupport::TestCase
  # Replace this with your real tests.
  test"to_csv" do
    report = EmailListEntry.to_csv()
    
    #lets parse our report
    parsed_report = CSV::Reader.parse(report)
    
    report = Array.new
    
    #we're going to push to multidimentional array
    #because we can't really poke around a StringIO buffer.
    parsed_report.each do |row|
      report << row
    end
    
    #assert our headers are present.
    assert_equal ['id', 'email', 'created at', 'updated at'], report[0]
    
    #assert that the second row is customer1 even though he has the biggest id
    assert_equal '3', report[1][0]
    assert_equal 'customer1@gmail.com', report[1][1]
    
    #verify that customer 2 was in the middle
    assert_equal '2', report[2][0]
    assert_equal 'customer2@gmail.com', report[2][1]
    
    #assert customer 3 is last
    assert_equal '1', report[3][0]
    assert_equal 'customer3@gmail.com', report[3][1]
    
    #verify we only had 3 entries (plus header)
    assert_equal 4, report.length
  end
end
