require 'test_helper'

class BusinessTest < ActiveSupport::TestCase
  fixtures :job_listings

  #test "regular business hours" do
  #  business = businesses(:bigbusiness)
  #
  #  wday = Time.now.wday
  #
  #  #test found business hours for today
  #  assert business.todays_hours
  ##  #toays hours match this day of the week
   # assert_equal wday, business.todays_hours.day_of_week
   # #they open @ 8am
   # assert_equal 8, business.todays_hours.open.hour
   # #they close @ 10pm
   # assert_equal 22, business.todays_hours.close.hour
  #end
  
  #test "override holiday hours" do
  #  business  = businesses(:bigbusiness)
  #
  #  #test special hours.
  #  special = BusinessHoursEntry.create({:override_date => Time.now, :open => '2001-01-01 12:00:00', :close => '2001-01-01 16:00:00', :business_id => 1})
  #  business.business_hours_entries << special
  #  business.save
    
    #assert day of week was nil
  #  assert_nil business.todays_hours.day_of_week
    
    #now we open @ noon
  #  assert_equal 12, business.todays_hours.open.hour
    #now we close @ 4
  #  assert_equal 16, business.todays_hours.close.hour
  #end
  
  test "current job listings" do
    
    business = businesses(:bigbusiness)
    
    jobs = business.active_job_listings

    #we only have one job entry.
    assert_equal 1, jobs.length
    #it's the cashiers job
    assert_equal 'Head Cashier', jobs[0].title

    #enable the other job.
    cook = JobListing.find_by_id(2)
    cook.enabled=true
    cook.save

    #test that cook shows up
    jobs = business.active_job_listings
    assert_equal 2, jobs.length
    #cook is now first because it was listed before the cashiers job.
    assert_equal 'Cook', jobs[0].title
  end
  
  test "current news items" do
    business = businesses(:bigbusiness)
    
    news = business.active_news_items
    
    #we only have one news item available right now.
    assert_equal 1, news.length
    assert_equal 'New Cook!', news[0].title
    
    item = NewsItem.find_by_id(1)
    item.enabled = true
    item.save
    
    item = NewsItem.find_by_id(3)
    item.enabled = true
    item.save
    
    news = business.active_news_items
    #we can read both news items
    assert_equal 3, news.length
    
    #assert line prep is pushed in the middle
    assert_equal "New Line Cook!", news[1].title
    
    
  end
end
