require 'test_helper'

class NotificationsTest < ActionMailer::TestCase
  test "contact_us" do
    @expected.subject = 'Notifications#contact_us'
    @expected.body    = read_fixture('contact_us')
    @expected.date    = Time.now

    assert_equal @expected.encoded, Notifications.create_contact_us(@expected.date).encoded
  end

end
