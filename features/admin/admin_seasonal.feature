Feature: Seasonal Promotion

  Background: Given I am logged in as an admin

  Scenario: Editing a seasonal promotion
    Given we have a seasonal promotion named "Memorial Day"
    When I go to the homepage
    And I click on "Edit Seasonal Promotion"
    And I fill in "Seasonal Promo" with "4th of July Sale"
    Then I should see "4th of July Sale" in the "Seasonal Promo"
