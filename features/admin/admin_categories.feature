Feature: Category

  Background: Given I am logged in as an admin

  Scenario: Add a new category
    Given I am on the admin category page
    And I press "Add Category"
    When I fill in "Name" with "Polynesian"
    And I press "Save"
    

  Scenario: Remove a category

  Scenario: Add a category to a store

  Scenario: Remove a category from a store
