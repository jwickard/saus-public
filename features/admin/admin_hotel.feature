Feature: Hotel

  Background: Given I am logged in as an admin

  Scenario: Adding a new hotel
    Given I am on the homepage
    And I press "Add New Hotel"
    Then I am on the new store page
    When I fill in "Blue Lagoon" as the "hotel name"
    And I fill in "10am - 10pm" as the "hotel hours"
    And I fill in "555-3456" as the "hotel phone number"
    And I fill in "http://www.bluelagoon.com" as the "hotel website"
    And I fill in "Lorem ipsum" as the "hotel details"
    And I click on "Save"
    Then I should be on that hotel page
    And I should see "Blue Lagoon"

  Scenario: Editing a current hotel
    Given a store exists called "BLUE LAGOON"
    And I am on the hotel page
    When I press "Edit"
    And I fill in "Blue Lagoon" as the "hotel name"
    And I press "Save"
    Then I should be on that hotel page
    And I should see "Blue Lagon"

  Scenario: Deleting a current hotel
    Given a store exists called "Blue Lagoon"
    And I am on the hotel page
    When I press "Delete"
    Then a store does not exist called "Blue Lagoon"

  Scenario: Adding picture to the hotel gallery

  Scenario: Removing picture from the hotel gallery


