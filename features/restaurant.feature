Feature: Restaurant

  Background: Given I am logged in as an admin

  Scenario: Adding a new restaurant
    Given I am on the homepage
    And I click on "Add New Restaurant"
    Then I am on the new store page
    When I fill in "Spago" as the "restaurant name"
    And I fill in "10am - 10pm" as the "restaurant hours"
    And I fill in "555-3456" as the "restaurant phone number"
    And I fill in "http://www.spago.com" as the "restaurant website"
    And I fill in "Lorem ipsum" as the "restaurant details"
    And I fill in "Lorem ipsum" as the "featured dishes"
    And I click on "Save"
    Then I should be on that restaurant page
    And I should see "Spago"

  Scenario: Editing a current restaurant
    Given a store exists called "SPAGE"
    And I am on the restaurant page
    When I click on "Edit"
    And I fill in "Spago" as the "restaurant name"
    And I click on "Save"
    Then I should be on that restaurant page
    And I should see "Spago"

  Scenario: Deleting a current restaurant
    Given a store exists called "Spago"
    And I am on the restaurant page
    When I click on "Delete"
    Then a store does not exist called "Spago"

  Scenario: Adding picture to the restaurant gallery

  Scenario: Removing picture from the restaurant gallery


