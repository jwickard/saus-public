Feature: Shop

  Background: Given I am logged in as an admin

  Scenario: Adding a new store
    Given I am on the homepage
    And I click on "Add New Store"
    Then I am on the new store page
    When I fill in "Gamestop" as the "store name"
    And I fill in "10am - 10pm" as the "store hours"
    And I fill in "555-3456" as the "store phone number"
    And I fill in "http://www.gamestop.com" as the "store website"
    And I fill in "Lorem ipsum" as the "store details"
    And I fill in "Lorem ipsum" as the "products we carry"
    And I click on "Save"
    Then I should be on that store page
    And I should see "Gamestop"

  Scenario: Editing a current store
    Given a store exists called "Gamestop"
    And I am on the store page
    When I click on "Edit"
    And I fill in "GameSTOP" as the "store name"
    And I click on "Save"
    Then I should be on that store page
    And I should see "GameSTOP"

  Scenario: Deleting a current store
    Given a store exists called "Gamestop"
    And I am on the store page
    When I click on "Delete"
    Then a store does not exist called "Gamestop"

  Scenario: Adding picture to the store gallery

  Scenario: Removing picture from the store gallery


