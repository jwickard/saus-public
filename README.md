# Shops At University Square #

Tenant / Store management for Shops At University Square in Rochester MN.  Project allowed tenants to manage their store information, news releases and products utilizing a convenient portal.

In production from 2009 - 2013.

### Tools ###

* Rails / jQuery / Prototype / Memcache
* Automated deployments with Capistrano / Rackspace
* Automated error reporting with Airbrake.io / New Relic