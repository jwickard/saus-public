require "spec"

describe Business do
  fixtures :businesses

  before(:each) do
    @valid_attributes = {}
    @business = businesses(:bigbusiness)
  end

  it "should create a new Business given valid attributes" do
    Business.create!(@valid_attributes)
  end

  it "should list available jobs" do
    jobs = business.active_job_listings
    jobs.length.should == 1
    jobs[0].title.should == 'Head Cashier'

    ##we only have one job entry.
    #assert_equal 1, jobs.length
    #it's the cashiers job
    #assert_equal 'Head Cashier', jobs[0].title

    #enable the other job.
    #cook = JobListing.find_by_id(2)
    #cook.enabled=true
    #cook.save

    #test that cook shows up
    #jobs = business.active_job_listings
    #assert_equal 2, jobs.length
    #cook is now first because it was listed before the cashiers job.
    #assert_equal 'Cook', jobs[0].title
  end
end