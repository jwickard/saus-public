require "spec"

describe MailingListSubmission do

  before(:each) do
    @valid_attributes = {:email => "jwickard@spitfirelab.com"}

    #setup the report
    report = MailingListSubmission.to_csv()

    #lets parse our report
    parsed_report = CSV::Reader.parse(report)

    @report = Array.new

    #we're going to push to multidimentional array
    #because we can't really poke around a StringIO buffer.
    parsed_report.each do |row|
      @report << row
    end
  end

  it "should create a new EmailListEntry given valid attributes" do
    MailingListSubmission.create!(@valid_attributes)
  end

  it "export records to csv file" do
    @report[0].should == ['id', 'email', 'created at', 'updated at']
    @report.length.should == 8

    #assert that the second row is customer1 even though he has the biggest id
    #assert_equal '3', report[1][0]
    #assert_equal 'customer1@gmail.com', report[1][1]

    #verify that customer 2 was in the middle
    #assert_equal '2', report[2][0]
    #assert_equal 'customer2@gmail.com', report[2][1]

    #assert customer 3 is last
    #assert_equal '1', report[3][0]
    #assert_equal 'customer3@gmail.com', report[3][1]

    #verify we only had 3 entries (plus header)
    #assert_equal 4, report.length
  end
end