require "spec"

describe JobListing do

  before(:each) do
    @valid_attributes = {:title => "Rails Opening", :description => "Doesn't Pay Anything", :business_id => 1}
  end

  it "should create a new Job given valid attributes" do
    JobListing.create!(@valid_attributes)
  end

  it "should default listed_at to same datetime as created_at if listed_at was blank" do
    job_listing = JobListing.create!(@valid_attributes)

    job_listing.listed_at.should == job_listing.created_at
  end
end