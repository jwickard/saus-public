class CreatePromos < ActiveRecord::Migration
  def self.up
    create_table :promos do |t|
      t.string :title
      t.string :slug
      t.text :description
      t.string :uri
      t.boolean :published
      t.string :image_file_name
      t.string :image_content_type
      t.string :image_file_size
      t.timestamp :image_updated_at

      t.timestamps
    end
  end

  def self.down
    drop_table :promos
  end
end
