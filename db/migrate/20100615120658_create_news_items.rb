class CreateNewsItems < ActiveRecord::Migration
  def self.up
    create_table :news_items do |t|
      t.string :title
      t.text :description
      t.boolean :enabled
      t.integer :business_id
      t.datetime :published_at

      t.timestamps
    end
  end

  def self.down
    drop_table :news_items
  end
end
