class CreateJobListings < ActiveRecord::Migration
  def self.up
    create_table :job_listings do |t|
      t.string :title
      t.text :description
      t.boolean :enabled
      t.integer :updated_by
      t.integer :business_id
      t.datetime :listed_at

      t.timestamps
    end
  end

  def self.down
    drop_table :job_listings
  end
end
