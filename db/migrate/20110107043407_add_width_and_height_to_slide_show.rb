class AddWidthAndHeightToSlideShow < ActiveRecord::Migration
  def self.up
    add_column :slide_shows, :width, :integer
    add_column :slide_shows, :height, :integer
  end

  def self.down
    remove_column :slide_shows, :height
    remove_column :slide_shows, :width
  end
end
