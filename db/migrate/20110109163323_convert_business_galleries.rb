class ConvertBusinessGalleries < ActiveRecord::Migration
  def self.up
    #find all businesses with null galleries & create them / convert their images.
    @businesses = Business.find(:all)

    @businesses.each do |business|
      if business.business_gallery.nil?
        write("#{business.id}:#{business.name} has no gallery, begin conversion")
        Business.transaction do
          business.business_gallery = BusinessGallery.create({:name => "#{business.name} Gallery", :enabled => true, :width => 535, :height => 304})
          write("created business gallery")
          business.save!
          write("saved main relationship")

          if not business.gallery_images.nil?
            write("Found #{business.gallery_images.size} to convert")
            business.gallery_images.each do |gallery_image|
              #business.business_gallery.slides = Slide.new({:slide_show_id => business.business_gallery.id, :doc => File.open(gallery_image.doc.path(:original)) })
              slide = Slide.new({:slide_show_id => business.business_gallery.id})
              slide.save!
              write("Created Slide: #{slide.id}")
              slide.doc = File.open(gallery_image.doc.path(:original))
              slide.save!
              write("Wrote New Slide Data")
              stale_image = GalleryImage.find(gallery_image.id)
              stale_image.destroy
              write("Removed Stale Reference")
            end
            
            business.business_gallery.save!
          else
            write("No gallery images to convert.")
          end
        end
      else
        write("converted gallery exists for #{business.name}")
      end
    end

    write("lastly we're creating the front page slide show.")
    #add the front page slideshow.
    front_page_gallery = CmsGallery.create({:enabled => true, :width => 960, :height => 352, :name => 'Front Page Gallery'})
    front_page_gallery.save!
  end

  def self.down
    raise ActiveRecord::IrreversibleMigration
  end
end
