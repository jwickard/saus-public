class AddSlideShowIdToDocument < ActiveRecord::Migration
  def self.up
    add_column :documents, :slide_show_id, :integer
  end

  def self.down
    remove_column :documents, :slide_show_id
  end
end
