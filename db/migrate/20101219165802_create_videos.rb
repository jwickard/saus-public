class CreateVideos < ActiveRecord::Migration
  def self.up
    create_table :videos do |t|
      t.string :name
      t.string :token
      t.string :full_url
      t.boolean :published

      t.timestamps
    end
  end

  def self.down
    drop_table :videos
  end
end
