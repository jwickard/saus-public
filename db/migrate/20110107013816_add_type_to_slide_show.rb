class AddTypeToSlideShow < ActiveRecord::Migration
  def self.up
    add_column :slide_shows, :type, :string
  end

  def self.down
    remove_column :slide_shows, :type
  end
end
