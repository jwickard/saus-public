class CreateBusinesses < ActiveRecord::Migration
  def self.up
    create_table :businesses do |t|
      t.string :type
      t.string :name
      t.string :slug
      t.text :description
      t.text :amenities
      t.text :details
      t.string :phone
      t.string :url
      t.string :location
      t.string :suite
      t.boolean :enabled

      t.timestamps
    end

    add_index :businesses, :type
    add_index :businesses, :slug, :unique => true
    add_index :businesses, :name
    add_index :businesses, [:enabled, :type]
  end

  def self.down
    drop_table :businesses
  end
end
