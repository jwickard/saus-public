class CreateProducts < ActiveRecord::Migration
  def self.up
    create_table :products do |t|
      t.integer :shop_id
      t.string :name
      t.boolean :enabled, :default => true
      t.integer :position

      t.timestamps
    end
  end

  def self.down
    drop_table :products
  end
end
