class CreateCmsPages < ActiveRecord::Migration
  def self.up
    create_table :cms_pages do |t|
      t.string :title
      t.string :keywords
      t.text :description
      t.string :slug
      t.text :body
      t.boolean :published

      t.timestamps
    end

    add_index :cms_pages, :slug, :unique => true
  end

  def self.down
    drop_table :cms_pages
  end
end
