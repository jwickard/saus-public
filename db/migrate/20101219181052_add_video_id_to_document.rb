class AddVideoIdToDocument < ActiveRecord::Migration
  def self.up
    add_column :documents, :video_id, :integer, :default => nil
  end

  def self.down
    remove_column :documents, :video_id
  end
end
