class CreateMailingListSubmissions < ActiveRecord::Migration
  def self.up
    create_table :mailing_list_submissions do |t|
      t.string :address
      t.boolean :subscribed
      t.timestamp :unsubscribed_at
      t.timestamp :subscribed_at

      t.timestamps
    end
  end

  def self.down
    drop_table :mailing_list_submissions
  end
end
