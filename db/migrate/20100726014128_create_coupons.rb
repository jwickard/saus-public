class CreateCoupons < ActiveRecord::Migration
  def self.up
    create_table :coupons do |t|
      t.integer :event_id
      t.string :name
      t.text :content

      t.timestamps
    end
  end

  def self.down
    drop_table :coupons
  end
end
