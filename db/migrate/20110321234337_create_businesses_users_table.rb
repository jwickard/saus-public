class CreateBusinessesUsersTable < ActiveRecord::Migration
  def self.up
    create_table :businesses_users, :id => false do |t|
      t.references :user
      t.references :business

      t.timestamps
    end

    #convert legacy relationships
    @users = User.find(:all)

    @users.each do |user|
      puts "starting #{user.email}"
      unless user.business_id.nil?
        puts "#{user.email} has business!"
        bizref = Business.find_by_id(user.business_id)
        puts "#successfully retrieved #{bizref.name} to associate with #{user.email}"

        user.businesses << bizref

        user.save!
      end
      puts "done processing #{user.email}"
    end
  end

  def self.down
    raise ActiveRecord::IrreversibleMigration
  end
end
