class CreateContactSubmissions < ActiveRecord::Migration
  def self.up
    create_table :contact_submissions do |t|
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :phone
      t.text :message
      t.boolean :newsletter

      t.timestamps
    end
  end

  def self.down
    drop_table :contact_submissions
  end
end
