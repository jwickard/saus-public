class AddPromoImageIdToPromo < ActiveRecord::Migration
  def self.up
    add_column :promos, :promo_image_id, :integer
  end

  def self.down
    remove_column :promos, :promo_image_id
  end
end
