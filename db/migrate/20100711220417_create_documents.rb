class CreateDocuments < ActiveRecord::Migration
  def self.up
    create_table :documents do |t|
      t.integer :business_id
      t.string :type
      t.string :doc_file_name
      t.string :doc_content_type
      t.integer :doc_file_size
      t.integer :position
      t.datetime :doc_updated_at

      t.timestamps
    end

    add_index :documents, :type
  end

  def self.down
    drop_table :documents
  end
end
