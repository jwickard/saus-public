class CreateBusinessCategoriesBusinesses < ActiveRecord::Migration
  def self.up
    create_table :business_categories_businesses, :id => false do |t|
      t.references :business
      t.references :business_category
      t.timestamps
    end
  end

  def self.down
    drop_table :business_categories_businesses
  end
end
