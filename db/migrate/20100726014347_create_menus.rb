class CreateMenus < ActiveRecord::Migration
  def self.up
    create_table :menus do |t|
      t.integer :restaurant_id
      t.string :name
      t.string :hours
      t.string :items
      t.text :description
      t.integer :position

      t.timestamps
    end
  end

  def self.down
    drop_table :menus
  end
end
