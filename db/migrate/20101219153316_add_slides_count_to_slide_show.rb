class AddSlidesCountToSlideShow < ActiveRecord::Migration
  def self.up
    add_column :slide_shows, :slides_count, :integer, :default => 0

    #add a proc to total up existing relations.
    SlideShow.reset_column_information
    SlideShow.find(:all).each do |show|
      SlideShow.update_counters show.id, :slides_count => show.slides.length
    end
  end

  def self.down
    remove_column :slide_shows, :slides_count
  end
end
