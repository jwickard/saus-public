class CreateBusinessHoursEntries < ActiveRecord::Migration
  def self.up
    create_table :business_hours_entries do |t|
      t.integer :day_of_week
      t.date :override_date
      t.time :open
      t.time :close
      t.integer :business_id

      t.timestamps
    end
  end

  def self.down
    drop_table :business_hours_entries
  end
end
