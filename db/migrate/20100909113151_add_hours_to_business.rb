class AddHoursToBusiness < ActiveRecord::Migration
  def self.up
    add_column :businesses, :hours, :text
  end

  def self.down
    remove_column :businesses, :hours
  end
end
