class CreateShopCategoriesShops < ActiveRecord::Migration
  def self.up
    create_table :shop_categories_shops, :id => false do |t|
      t.references :shop
      t.references :shop_category
      t.timestamps
    end
  end

  def self.down
    drop_table :shop_categories_shops
  end
end
