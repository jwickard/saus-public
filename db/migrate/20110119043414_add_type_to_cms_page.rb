class AddTypeToCmsPage < ActiveRecord::Migration
  def self.up
    add_column :cms_pages, :type, :string
  end

  def self.down
    remove_column :cms_pages, :type
  end
end
