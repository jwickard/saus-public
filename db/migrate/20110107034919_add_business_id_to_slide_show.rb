class AddBusinessIdToSlideShow < ActiveRecord::Migration
  def self.up
    add_column :slide_shows, :business_id, :integer
  end

  def self.down
    remove_column :slide_shows, :business_id
  end
end
