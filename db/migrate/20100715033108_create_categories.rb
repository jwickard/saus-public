class CreateCategories < ActiveRecord::Migration
  def self.up
    create_table :categories do |t|
      t.string :name
      t.string :slug
      t.string :type

      t.timestamps
    end

    add_index :categories, :slug, :unique => true
    add_index :categories, :type
  end

  def self.down
    drop_table :categories
  end
end
