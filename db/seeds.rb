# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#   
#   cities = City.create([{ :name => 'Chicago' }, { :name => 'Copenhagen' }])
#   Major.create(:name => 'Daley', :city => cities.first)
roles = Role.create([
  {:name => 'user', :display => 'User'}, 
  {:name => 'store_admin', :display => 'Store Admin'},
  {:name => 'mall_admin', :display => 'Mall Admin'},
  {:name => 'root', :display => 'Root'}
  ])

users = User.create([{:email => 'jwickard@gmail.com', :password => 'smokey', :password_confirmation => 'smokey'},
                     {:email => 'store@admin.com', :password =>'storeadmin', :password_confirmation => 'storeadmin'},
                     {:email => 'mall@admin.com', :password => 'malladmin', :password_confirmation => 'malladmin'}])

users[0].roles << roles[3]

users[0].save

users[1].roles << roles[1]

users[1].save

users[2].roles << roles[2]

users[2].save

shops = Shop.create([{
          :name => 'Sopra Sotto',
          :description => 'Sopra Sotto Store Description',
          :details => '<p>Sopra Sotto presents handcrafted artisanal products from Italy for the Home, Kitchen and giftgiving. Everything from handpainted Italian ceramics to crystal, table linens, flatware, volcanic stone tables, pasta machines, fine chocolate and gourmet food imports such as olive oil, balsamic vinegar and pasta.</p><p>Handbound leather journals and portfolios, stationery and books, art and photography and Bath and Body Products are also offered. Sopra Sotto also offers Cooking Lessons and wine seminars as well as an Espresso Bar serving Illy coffee, pastries and light lunches.</p>',
          :phone => '5072525522',
          :url => 'http://www.sopra-sotto.com',
          :suite => '104',
          :enabled => true },
                     
         {:name => 'Express Mens',
          :description => 'Express Mens Store Description',
          :details => '<p>A paragraph about Mens fashion.</p><p>A paragraph about fashion in Rochester</p>',
          :phone => '5072345920',
          :url => 'http://expressmens.com',
          :suite => '208',
          :enabled => true
         }])


#TODO figure out how to use the saved values in seeds
for p in ['Olive Oil', 'ImportedFoods', 'Dinnerware', 'Serverware', 'Crystal', 'Flatware', 'Decorative Accessories', 'Bath & Body', 'Leather', 'Kitchenware', 'Culinary Instruction', 'Terracotta', 'Tables', 'Photo Albums']
    Product.create({:shop_id => shops[0].id, :name => p, :enabled => true})
end

%w{Socks Shoes Slacks Belts Sweaters Jackets Briefs Watches Glasses}.each do |p|
  Product.create({:shop_id => shops[1].id, :name => p, :enabled => true})
end

for c in ['Books', 'Cards']
  shops[0].categories << ShopCategory.create({:name => c, :slug => c.downcase})
end

shops[0].save

%w{Apparel Apparel/Mens Accessories}.each do |c|
  shops[1].categories << ShopCategory.create({:name => c, :slug => c.downcase})
end

shops[1].save

restaurants = Restaurant.create({
          :name => 'Applebees',
          :description => 'Eating Good In The Neighborhood',
          :details => "<p>Applebee's is the largest casual dining chain in the world, with locations throughout the U.S. and many countries worldwide. We take pride in having a friendly, welcoming, neighborhood environment for both our staff and guests that makes everyone enjoy their Applebee's experience.</p>",
          :phone => '5072039286',
          :url => 'http://applebees.com',
          :enabled => true
        })

%w{Appetizers Dinner Lunch}.each do |c|
  restaurants.categories << RestaurantCategory.create({:name => c, :slug => c.downcase})
end

restaurants.save

# Create menu for the restaurant
menu = Menu.new(:restaurant_id => restaurants.id,
                :name => "Lunch",
                :hours => "10am - 1pm",
                :items => "Sandwiches, Burgers, and Salads",
                :description => "Come in for our artisanal selection.",
                :position => 1)

menu.save

menu = Menu.new(:restaurant_id => restaurants.id,
                :name => "Dinner",
                :hours => "5pm - Midnight",
                :items => "Everything that isn't in a hot dog.",
                :description => "Yup, this stuff is artesinal too.",
                :position => 2)

menu.save

# Create some events

event = Event.new(:business_id => shops[0].id,
                  :name => "Be Fabulous Event",
                  :start_date => "2010-06-25",
                  :end_date => "2010-10-25",
                  :description => "You should come in and feel fabulous.")

event.save

event = Event.new(:business_id => shops[0].id,
                  :name => "Apple Back to School Sale",
                  :start_date => "2010-08-25",
                  :end_date => "2010-09-25",
                  :description => "Come in for back to school savings.")

event.save

# Add a Coupon to one of the events.
coupon = Coupon.new(:event_id => event.id,
                    :name => "5&#38; Off All Shoes",
                    :content => "5&#38; off all shoes we carry.")

coupon.save

random_shops = Array.new
#create a bunch of random business
%w{BizA BizB BizC BizD BizE BizF BizG BizH BizI BizJ BizK BizL BizM BizN BizO BizP BizQ BizR BizS BizT BizU BizV BizW BizX BizY BizZ}.each_with_index do |biz, i|
  random_shops << Shop.create(
          {:name => biz,
          :description => "#{biz} Store Description",
          :details => "<p>A paragraph about #{biz}</p><p>A paragraph about fashion in the town that #{biz} was founded in</p>",
          :phone => '5072345920',
          :url => "http://#{biz}.com",
          :suite => (300 + i).to_s,
          :enabled => true
         })
end

shop_count = Shop.find(:all).length

random_shops.each do |biz|
  random_limit = 1 + rand(shop_count).to_i
  categories = ShopCategory.find(:all, :order => 'random()', :limit => random_limit)
  biz.categories << categories
  biz.save
end

Hotel.create(
          {:name => 'Doubletree',
          :description => "Doubletree Store Description",
          :details => "<p>A paragraph about Doubletree</p><p>A paragraph about fashion in the town that Doubletree was founded in</p>",
          :phone => '5072345920',
          :amenities => '<p>All sorts of stuff you cant even imagine because it is just so awesome.</p>',
          :url => "http://doubletree.com",
          :suite => nil,
          :enabled => true
         })

#:order => (random())
#1 + rand(count)

