# This file is auto-generated from the current state of the database. Instead of editing this file, 
# please use the migrations feature of Active Record to incrementally modify your database, and
# then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your database schema. If you need
# to create the application database on another system, you should be using db:schema:load, not running
# all the migrations from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20110321234337) do

  create_table "audits", :force => true do |t|
    t.integer  "auditable_id"
    t.string   "auditable_type"
    t.integer  "user_id"
    t.string   "user_type"
    t.string   "username"
    t.string   "action"
    t.text     "changes"
    t.integer  "version",        :default => 0
    t.datetime "created_at"
  end

  add_index "audits", ["auditable_id", "auditable_type"], :name => "auditable_index"
  add_index "audits", ["created_at"], :name => "index_audits_on_created_at"
  add_index "audits", ["user_id", "user_type"], :name => "user_index"

  create_table "business_categories_businesses", :id => false, :force => true do |t|
    t.integer  "business_id"
    t.integer  "business_category_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "business_hours_entries", :force => true do |t|
    t.integer  "day_of_week"
    t.date     "override_date"
    t.time     "open"
    t.time     "close"
    t.integer  "business_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "business_schedules", :force => true do |t|
    t.string   "name"
    t.integer  "day_of_week"
    t.time     "open_at"
    t.time     "close_at"
    t.date     "exception_date"
    t.boolean  "closed"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "businesses", :force => true do |t|
    t.string   "type"
    t.string   "name"
    t.string   "slug"
    t.text     "description"
    t.text     "amenities"
    t.text     "details"
    t.string   "phone"
    t.string   "url"
    t.string   "location"
    t.string   "suite"
    t.boolean  "enabled"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "hours"
  end

  add_index "businesses", ["enabled", "type"], :name => "index_businesses_on_enabled_and_type"
  add_index "businesses", ["name"], :name => "index_businesses_on_name"
  add_index "businesses", ["slug"], :name => "index_businesses_on_slug", :unique => true
  add_index "businesses", ["type"], :name => "index_businesses_on_type"

  create_table "businesses_users", :id => false, :force => true do |t|
    t.integer  "user_id"
    t.integer  "business_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "categories", :force => true do |t|
    t.string   "name"
    t.string   "slug"
    t.string   "type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "categories", ["slug"], :name => "index_categories_on_slug", :unique => true
  add_index "categories", ["type"], :name => "index_categories_on_type"

  create_table "cms_pages", :force => true do |t|
    t.string   "title"
    t.string   "keywords"
    t.text     "description"
    t.string   "slug"
    t.text     "body"
    t.boolean  "published"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "type"
  end

  add_index "cms_pages", ["slug"], :name => "index_cms_pages_on_slug", :unique => true

  create_table "contact_submissions", :force => true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "phone"
    t.text     "message"
    t.boolean  "newsletter"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "coupons", :force => true do |t|
    t.integer  "event_id"
    t.string   "name"
    t.text     "content"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "delayed_jobs", :force => true do |t|
    t.integer  "priority",   :default => 0
    t.integer  "attempts",   :default => 0
    t.text     "handler"
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], :name => "delayed_jobs_priority"

  create_table "documents", :force => true do |t|
    t.integer  "business_id"
    t.string   "type"
    t.string   "doc_file_name"
    t.string   "doc_content_type"
    t.integer  "doc_file_size"
    t.integer  "position"
    t.datetime "doc_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "slide_show_id"
    t.integer  "video_id"
  end

  add_index "documents", ["type"], :name => "index_documents_on_type"

  create_table "events", :force => true do |t|
    t.string   "name"
    t.integer  "business_id"
    t.date     "start_date"
    t.date     "end_date"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "job_listings", :force => true do |t|
    t.string   "title"
    t.text     "description"
    t.boolean  "enabled"
    t.integer  "updated_by"
    t.integer  "business_id"
    t.datetime "listed_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "mailing_list_submissions", :force => true do |t|
    t.string   "address"
    t.boolean  "subscribed"
    t.datetime "unsubscribed_at"
    t.datetime "subscribed_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "menus", :force => true do |t|
    t.integer  "restaurant_id"
    t.string   "name"
    t.string   "hours"
    t.string   "items"
    t.text     "description"
    t.integer  "position"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "news_items", :force => true do |t|
    t.string   "title"
    t.text     "description"
    t.boolean  "enabled"
    t.integer  "business_id"
    t.datetime "published_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "products", :force => true do |t|
    t.integer  "shop_id"
    t.string   "name"
    t.boolean  "enabled",    :default => true
    t.integer  "position"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "promos", :force => true do |t|
    t.string   "title"
    t.string   "slug"
    t.text     "description"
    t.string   "uri"
    t.boolean  "published"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.string   "image_file_size"
    t.datetime "image_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "promo_image_id"
  end

  create_table "roles", :force => true do |t|
    t.string   "name"
    t.string   "display"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "roles_users", :id => false, :force => true do |t|
    t.integer  "user_id"
    t.integer  "role_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "shop_categories_shops", :id => false, :force => true do |t|
    t.integer  "shop_id"
    t.integer  "shop_category_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "slide_shows", :force => true do |t|
    t.string   "name"
    t.string   "slug"
    t.boolean  "enabled"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "slides_count", :default => 0
    t.string   "type"
    t.integer  "business_id"
    t.integer  "width"
    t.integer  "height"
  end

  create_table "users", :force => true do |t|
    t.string   "email",                                 :null => false
    t.string   "crypted_password",                      :null => false
    t.string   "password_salt",                         :null => false
    t.string   "persistence_token",                     :null => false
    t.string   "single_access_token",                   :null => false
    t.string   "perishable_token",                      :null => false
    t.integer  "login_count",         :default => 0,    :null => false
    t.integer  "failed_login_count",  :default => 0,    :null => false
    t.datetime "last_request_at"
    t.datetime "current_login_at"
    t.datetime "last_login_at"
    t.string   "current_login_ip"
    t.string   "last_login_ip"
    t.integer  "business_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "enabled",             :default => true
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true

  create_table "videos", :force => true do |t|
    t.string   "name"
    t.string   "token"
    t.string   "full_url"
    t.boolean  "published"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
